/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diningphilsophers;

/**
 *
 * @author davidm
 */
public class Resources {
    final static Object obj = new Object();
    final static Chopstick[] chopsticks = new Chopstick[5];

    public static void setTheTable(int number) {
        for (int i = 0; i < number; i++) {
            chopsticks[i] = new Chopstick(i);
        }
    }

    public static class Chopstick {

        volatile boolean inUse;
        private final int id;

        public Chopstick(int id) {
            this.id = id;
            inUse = false;
        }
    }

    public static Chopstick[] fetchChopsticks(int left, int right) {
        while (true) {
            if (!chopsticks[left].inUse && !chopsticks[right].inUse) {
                synchronized (Resources.obj) {
                    Chopstick[] cs = new Chopstick[2];
                    cs[0] = chopsticks[left];
                    cs[0].inUse = true;
                    cs[1] = chopsticks[right];
                    cs[1].inUse = true;
                    return cs;
                }
            }
        }
    }
}
