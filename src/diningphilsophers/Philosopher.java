/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diningphilsophers;

import java.util.Random;

/**
 *
 * @author davidm
 */
public class Philosopher extends Thread {
    private final String name;
    private Resources.Chopstick left;
    private Resources.Chopstick right;
    private final int id;
    private volatile boolean isRunning;

    private void doStuff(long randomTime) {
        try {
            Thread.sleep(randomTime);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private void updateState(State state) {
        //System.out.println("The Philosopher "+name+" is now: " + state.toString());
        currentState = state;
    }

    private void releaseChopsticks() {
        left.inUse = false;
        right.inUse = false;
    }

    private void requestChopsticks() {
        Resources.Chopstick[] cs = Resources.fetchChopsticks(id,id+1%5);
        left = cs[0];
        right = cs[1];
    }

    void printStatus() {
        System.out.println("The Philosopher "+name+" is: " + currentState.toString());
    }

    private enum State {
        HUNGRY, THINKING, EATING
    };
    private State currentState;

    public Philosopher(int id, String name) {
        isRunning = true;
        this.name = name;
        this.id = id;
        currentState = State.THINKING;
    }

    @Override
    public void run() {
        while (isRunning) {
            if (currentState == State.THINKING) {
                doStuff(getRandomTime());
                updateState(State.HUNGRY);
            }
            if (currentState == State.EATING) {
                doStuff(getRandomTime());
                releaseChopsticks();
                updateState(State.THINKING);
            }
            if (currentState == State.HUNGRY) {
                requestChopsticks();
                updateState(State.EATING);
            }
        }
    }

    public long getRandomTime() {
        Random rand = new Random();
        return (rand.nextInt(7)+3) * 1000;
    }
}
