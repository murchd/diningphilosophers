/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diningphilsophers;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author davidm
 */
public class DiningRoom {

    final int numberOfPhilsophers = 5;
    Philosopher[] guests = new Philosopher[5];
    String[] names = new String[]{"David", "Anna", "Bob", "John", "Beck"};

    public static void main(String args[]) {
        new DiningRoom().run();
    }
    private volatile boolean isRunning = true;

    public void run() {

        Resources.setTheTable(numberOfPhilsophers);

        for (int i = 0; i < numberOfPhilsophers; i++) {
            guests[i] = new Philosopher(i, names[i]);
            guests[i].start();
        }
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                isRunning = false;
                for (int i = 0; i < numberOfPhilsophers; i++) {
                    guests[i].interrupt();
                }
            }
        }));
        while (isRunning) {
            System.out.println("-------------------------------------------");
            for (int i = 0; i < numberOfPhilsophers; i++) {
                guests[i].printStatus();
            }
            System.out.println("-------------------------------------------");
            try {
                Thread.sleep(1000L); //tick status update
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

    }
}
